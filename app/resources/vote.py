from datetime import datetime, timedelta

from flask_jwt_extended import (
    create_access_token,
    get_jwt_claims,
    get_jwt_identity,
    get_raw_jwt,
    jwt_required,
)
from flask_restful import Resource, reqparse

from app.blacklist import BLACKLIST
from app.models.concept import RoleModel
from app.models.statistic import StatisticModel
from share.macros import Algo


class Vote(Resource):
    parser = reqparse.RequestParser()

    parser.add_argument(
        "table", type=str, required=True, help="you have to provide a cible talbe (str)"
    )
    parser.add_argument(
        "value_as_string",
        type=str,
        required=True,
        help="you have to provide a value (str)",
    )

    @classmethod
    @jwt_required
    def put(cls, _id):
        """
        check if value is already provided
        # yes => nothing, no => write and suppress reverse vote
        """
        m_user_id = get_jwt_identity()
        if not RoleModel.vote(_id, m_user_id):
            return {"Message": "Unauthorized"}, 401

        data = cls.parser.parse_args()
        if data["table"] == "concept_relationship":
            values = []
            values = StatisticModel.one_user_for_one_stat(
                _id, "VOTE_MAPPING", m_user_id, None
            )
            for v in values:
                if data["value_as_string"] != v.m_value_as_string:
                    v.m_invalid_reason = "D"
                    v.m_valid_end_datetime = datetime.now()
                    v.upserting()
            stat_vote_mapping = StatisticModel(
                _id,
                "VOTE_MAPPING",
                Algo.MANUAL_VOTE_MAPPING.value,
                m_user_id,
                m_value_as_string=data["value_as_string"],
            )
            stat_vote_mapping.upserting()
            return {"message": "New vote"}, 200

    @classmethod
    @jwt_required
    def delete(cls, _id):
        m_user_id = get_jwt_identity()
        if not RoleModel.vote(_id, m_user_id):
            return {"Message": "Unauthorized"}, 401

        data = cls.parser.parse_args()
        if data["table"] == "concept_relationship":
            values = []
            values = StatisticModel.one_user_for_one_stat(
                _id, "VOTE_MAPPING", m_user_id, None
            )
            for v in values:
                if data["value_as_string"] == v.m_value_as_string:
                    v.m_invalid_reason = "D"
                    v.m_valid_end_datetime = datetime.now()
                    v.upserting()
            return {"message": "Vote deleted"}, 200
