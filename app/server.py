from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_restful import Api

from app.blacklist import BLACKLIST
from app.resources.concept import Concept, ConceptList
from app.resources.concept_relationship import ConceptRelation, ConceptRelationList
from app.resources.project import Project, ProjectList
from app.resources.user import User, UserList, UserLogin, UserLogout
from app.resources.vote import Vote

# Create app and API
app = Flask(__name__)
app.config.from_object("conf_files.config")
app.config["CORS_HEADERS"] = "Content-Type"
api = Api(app)

# open cross url
cors = CORS(app, ressources={r"*": {"origins": "*"}})

# links flask_jwt_extended to our app
jwt = JWTManager(app)

# Create a function that will be called whenever create_access_token
# is used. It will take whatever object is passed into the
# create_access_token method, and lets us define what custom claims
# should be added to the access token.
@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {
        "m_firstname": user.m_firstname,
        "m_lastname": user.m_lastname,
        "m_username": user.m_username,
        "m_email": user.m_email,
        "m_is_admin": user.m_is_admin,
    }


# Create a function that will be called whenever create_access_token
# is used. It will take whatever object is passed into the
# create_access_token method, and lets us define what the identity
# of the access token should be.
@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.id


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    return decrypted_token["jti"] in BLACKLIST


# All the endpoints
api.add_resource(UserList, "/users")
api.add_resource(User, "/user/<string:m_username>")
api.add_resource(UserLogin, "/login")
api.add_resource(UserLogout, "/logout")
api.add_resource(ConceptList, "/concepts")
api.add_resource(Concept, "/concept/<int:_id>")
api.add_resource(ConceptRelation, "/concept_relationship/<int:_id>")
api.add_resource(ConceptRelationList, "/relationships")
api.add_resource(Vote, "/vote/<int:_id>")
api.add_resource(ProjectList, "/projects")
api.add_resource(Project, "/project/<string:m_username>")

if __name__ == "__main__":
    from app.db import db

    # logging.basicConfig(filename='error.log',level=logging.DEBUG)

    db.init_app(app)
    app.run(port=5000, debug=True)
