import logging

from googletrans import Translator as GoogleTransTranslator
from translate import Translator as TranslateTranslator


logging.getLogger().setLevel(level=logging.WARN)
ISO_OMOP_LANGUAGES = {
    # "zh-cn": 4182948,  # Chinese language
    # "da": 4180183,  # Danish language
    # "nl": 4182503,  # Dutch language
    "en": 4180186,  # English language,
    "fr": 4180190,  # French language
    # "de": 4182504,  # German language
    # "el": 4180189,  # Greek language
    # "ga": 4180182,  # Irish Gaelic language
    # "it": 4182507,  # Italian language
    # "ja": 4181524,  # Japanese language
    # "ko": 4175771,  # Korean language
    # "pl": 4182515,  # Polish language
    # "pt": 4181536,  # Portuguese language
    # "ru": 4181539,  # Russian language
    # "es": 4182511,  # Spanish language
}

TRANSLATION_AVAILABLE_ALGOS = ["GOOGLETRANS", "TRANSLATE_TRANSLATOR"]

API_DEAD = "All the translation API are dead - Wait"

MYMEMORY_ERROR = "MYMEMORY WARNING: YOU USED ALL AVAILABLE FREE TRANSLATIONS FOR TODAY"


def string_cleaner(string_to_clean, to_erase):
    for ch in to_erase:
        if ch in string_to_clean:
            string_to_clean = string_to_clean.replace(ch, " ")
    return string_to_clean


def googletrans_translator(text, src, dest):
    #  Google Translate Ajax API
    translation = API_DEAD
    try:
        translation = GoogleTransTranslator().translate(text, src=src, dest=dest).text
    except Exception as e:
        logging.error(e)
    return translation


def translate_translator(text, src, dest):
    # Microsoft Translation API and Translated MyMemory API
    # pip install translate
    translation = API_DEAD
    try:
        translation = TranslateTranslator(from_lang=src, to_lang=dest).translate(text)
        if MYMEMORY_ERROR in translation:
            translation = API_DEAD
    except Exception as e:
        logging.error(e)
    return translation


def translate(string_to_translate, string_src, string_dest, list_available_algo="ALL"):
    if list_available_algo == "ALL":
        list_available_algo = TRANSLATION_AVAILABLE_ALGOS.copy()

    concept_name_clean = string_cleaner(string_to_translate.lower(), ["_", "/"])

    algos = {
        "GOOGLETRANS": googletrans_translator,
        "TRANSLATE_TRANSLATOR": translate_translator,
    }
    ret = API_DEAD
    for algo in list_available_algo:
        logging.warning("Looking for %s", algo)
        ret = algos[algo](concept_name_clean, string_src, string_dest)
        if ret == API_DEAD:
            list_available_algo.remove(algo)
        else:
            return ret
    return ret


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("string_to_translate", type=str)
    parser.add_argument("string_src", type=str)
    parser.add_argument("string_dest", type=str)
    parser.add_argument("list_available_algo", type=str)
    args = parser.parse_args()

    string_to_translate = args.string_to_translate
    string_src = args.string_src
    string_dest = args.string_dest
    list_available_algo = args.list_available_algo

    result = translate(string_to_translate, string_src, string_dest, list_available_algo.split(" "))
    print(result, end="")
