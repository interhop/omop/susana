import logging
import time

from asynchron.jobs.solr_sync import PostgresToSolr
from conf_files.config import (
    INIT_SOLR_AT_STARTUP,
    JOB_FREQUENCY_IN_SECONDS,
    POSTGRES,
    SOLR_CORE,
    SOLR_CSV_FILEPATH,
    SOLR_HOST,
    SOLR_PASSWORD,
    SOLR_PORT,
    SOLR_USER,
)
from pysolrwrapper.loader_postgres.postgres_utils import PostgresConf
from pysolrwrapper.loader_solr.solr_utils import SolrConf

logging.getLogger().setLevel(level=logging.WARN)


postgres_conf = PostgresConf(**POSTGRES)

solr_conf = SolrConf(password=SOLR_PASSWORD, host=SOLR_HOST, user=SOLR_USER, port=SOLR_PORT, core=SOLR_CORE)

# SOLR SYNC
if INIT_SOLR_AT_STARTUP:
    solr_sync = PostgresToSolr(
        tmp_csv_path=SOLR_CSV_FILEPATH,
        solr_conf=solr_conf,
        postgres_conf=postgres_conf,
        from_scratch=True,
        debug=True,
    )
    solr_sync.run()

while True:
    time.sleep(JOB_FREQUENCY_IN_SECONDS)

    # 0: TO BE DONE
    # 1: DOING
    # 2: DONE
    # 3: ERROR

    # SOLR SYNC
    solr_sync = PostgresToSolr(
        tmp_csv_path=SOLR_CSV_FILEPATH,
        solr_conf=solr_conf,
        postgres_conf=postgres_conf,
        from_scratch=False,
        debug=True,
    )
    solr_sync.run()
