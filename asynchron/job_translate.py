import logging
import time

from asynchron.bulk_trans.all_trans import synonym_translation_api_extend
from asynchron.jobs.send_relations_to_relative_ids import (
    extend_relations_for_one_concept,
)
from conf_files.config import (
    JOB_FREQUENCY_IN_SECONDS,
)
from share.macros import TRANSLATION_AVAILABLE_ALGOS

logging.getLogger().setLevel(level=logging.WARN)

list_available_algo = TRANSLATION_AVAILABLE_ALGOS.copy()


# 0: TO BE DONE
# 1: DOING
# 2: DONE
# 3: ERROR
while True:
    time.sleep(JOB_FREQUENCY_IN_SECONDS)

    # EXTENDED TRANSLATION JOB
    synonym_translation_api_extend(list_available_algo)
    # EXTEND SYNONYMS AND RELATIVES TO PROJECTS (FOR ONE CONCEPT)
    extend_relations_for_one_concept()
