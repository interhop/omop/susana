GOOGLE_OMOP_LANGUAGES = {
    #    'af' : 4180185 	,  # Afrikaans language
    #    'sq' : 4182497 	,  # Albanian language
    #    'am' : 4182354 	,  # Amharic language
    #    'ar' : 4181374 	,  # Arabic language
    #    'hy' : 4180063 	,  # Armenian language
    #    'az' : 4181383 	,  # Azerbaijani language
    #    'eu' : 4175770 	,  # Basque language
    #    'be' : 4180194 	,  # Belorussian language
    #    'bn' : 4052786 	,  # Bengali language
    #    'bs' : 40481563 	,  # Bosnian language
    #    'bg' : 4182512 	,  # Bulgarian language
    #    'ca' : 4181535 	,  # Catalan language
    "zh-cn": 4182948,  # Chinese language
    #    'co' : 4232095 	,  # Corsican language
    #    'hr' : 40481973 	,  # Croatian language
    #    'cs' : 4180197 	,  # Czech language
    "da": 4180183,  # Danish language
    "nl": 4182503,  # Dutch language
    "en": 4180186,  # English language, DELETED in this list because manually put in concept.py file
    #    'eo' : 4178686 	,  # Esperanto
    #    'et' : 4183943 	,  # Estonian language
    #    'fi' : 4181730 	,  # Finnish language
    "fr": 4180190,  # French language
    #    'fy' : 4180188 	,  # Frisian language
    #    'gl' : 4261080	,  # Galician language
    #    'ka' : 4182481	,  # Georgian language
    "de": 4182504,  # German language
    "el": 4180189,  # Greek language
    #    'gu' : 4059174	,  # Gujarati language
    #    'ht' : 44802876	,  # Haitian Creole language
    #    'ha' : 4178556	,  # Hausa language
    #    'haw' : 4181557	,  # Hawaiian language
    #    'iw' : 4180047	,  # Hebrew language
    #    'hi' : 4052346 	,  # Hindi language
    #    'hmn' :  46270898	,  # Hmong language
    #    'hu' : 4183945	,  # Hungarian language
    #    'is' : 4175776	,  # Icelandic language
    #    'id' : 4183663	,  # Indonesian language
    #    'ga' : 4180182	,  # Irish Gaelic language
    "it": 4182507,  # Italian language
    "ja": 4181524,  # Japanese language
    #    'jw' : 4175914	,  # Javanese language
    #    'kk' : 4182474	,  # Kazakh language
    #    'km' : 4183770	,  # Khmer language
    "ko": 4175771,  # Korean language
    #    'ku' : 4175909	,  # Kurdish language
    #    'lo' : 4182952	,  # Lao language
    #    'la' : 4181534	,  # Latin language
    #    'lv' : 4180064	,  # Latvian language
    #    'lt' : 4175772	,  # Lithuanian language
    #    'lb' : 46285414	,  # Main spoken language Luxembourgish
    #    'mk' : 4175905	,  # Macedonian language
    #    'mg' : 4180215	,  # Malagasy language
    #    'ms' : 4183666	,  # Malay language
    #    'ml' : 4180058	,  # Malayalam language
    #    'mt' : 4175735	,  # Maltese language
    #    'mi' : 4180223	,  # Maori language
    #    'mr' : 4182519	,  # Marathi language
    #    'mn' : 4182357	,  # Mongolian language
    #    'my' : 4181727	,  # Burmese language
    #    'ne' : 4175908	,  # Nepali language
    #    'no' : 4181530	,  # Norwegian language
    #    'fa' : 4183659	,  # Persian language
    #    'pl' : 4182515	,  # Polish language
    #    'pt' : 4181536	,  # Portuguese language
    #    'pa' : 4059175	,  # Punjabi language
    #    'ro' : 4181537	,  # Rumanian language
    #    'ru' : 4181539	,  # Russian language
    #    'sm' : 4181558	,  # Samoan language
    #    'gd' : 4181529	,  # Scottish Gaelic language
    #    'sr' : 40483799	,  # Serbian language
    #    'sn' : 4175936	,  # Shona language
    #    'sd' : 4181545	,  # Sindhi language
    #    'si' : 4183657	,  # Sinhalese language
    #    'sk' : 4181540	,  # Slovak language
    #    'sl' : 4182513	,  # Slovenian language
    #    'so' : 4182350	,  # Somali language
    "es": 4182511,  # Spanish language
    #    'su' : 4181554	,  # Sundanese language
    #    'sw' : 4181698	,  # Swahili language
    #    'sv' : 4175777	,  # Swedish language
    #    'tg' : 4181547	,  # Tajik language
    #    'ta' : 4181521	,  # Tamil language
    #    'te' : 4175767	,  # Telugu language
    #    'th' : 4183934	,  # Thai language
    #    'tr' : 4175744	,  # Turkish language
    #    'uk' : 4175904	,  # Ukrainian language
    #    'ur' : 4059788	,  # Urdu language
    #    'uz' : 4175743	,  # Uzbek language
    #    'vi' : 4181526	,  # Vietnamese language
    #    'cy' : 4175774	,  # Welsh language
    #    'xh' : 4180355	,  # Xhosa language
    #    'yi' : 4181531	,  # Yiddish language
    #    'yo' : 4183798	,  # Yoruba language
    #    'zu' : 4181699	,  # Zulu language
    #    'he' : 4180047	,  # Hebrew language
}

GOOGLE_LANGUAGES = [
    "af",  # 4180185 	-- Afrikaans language
    "sq",  # 4182497 	-- Albanian language
    "am",  # 4182354 	-- Amharic language
    "ar",  # 4181374 	-- Arabic language
    "hy",  # 4180063 	-- Armenian language
    "az",  # 4181383 	-- Azerbaijani language
    "eu",  # 4175770 	-- Basque language
    "be",  # 4180194 	-- Belorussian language
    "bn",  # 4052786 	-- Bengali language
    "bs",  # 40481563 	-- Bosnian language
    "bg",  # 4182512 	-- Bulgarian language
    "ca",  # 4181535 	-- Catalan language
    "zh-cn",  # 4182948 	-- Chinese language
    "co",  # 4232095 	-- Corsican language
    "hr",  # 40481973 	-- Croatian language
    "cs",  # 4180197 	-- Czech language
    "da",  # 4180183 	-- Danish language
    "nl",  # 4182503	-- Dutch language
    "en",  # 4180186 	-- English language
    "eo",  # 4178686 	-- Esperanto
    "et",  # 4183943 	-- Estonian language
    "fi",  # 4181730 	-- Finnish language
    "fr",  # 4180190 	-- French language
    "fy",  # 4180188 	-- Frisian language
    "gl",  # 4261080	-- Galician language
    "ka",  # 4182481	-- Georgian language
    "de",  # 4182504	-- German language
    "el",  # 4180189	-- Greek language
    "gu",  # 4059174	-- Gujarati language
    "ht",  # 44802876	-- Haitian Creole language
    "ha",  # 4178556	-- Hausa language
    "haw",  # 4181557	-- Hawaiian language
    "iw",  # 4180047	-- Hebrew language
    "hi",  # 4052346 	-- Hindi language
    "hmn",  # 46270898	-- Hmong language
    "hu",  # 4183945	-- Hungarian language
    "is",  # 4175776	-- Icelandic language
    "id",  # 4183663	-- Indonesian language
    "ga",  # 4180182	-- Irish Gaelic language
    "it",  # 4182507	-- Italian language
    "ja",  # 4181524	-- Japanese language
    "jw",  # 4175914	-- Javanese language
    "kk",  # 4182474	-- Kazakh language
    "km",  # 4183770	-- Khmer language
    "ko",  # 4175771	-- Korean language
    "ku",  # 4175909	-- Kurdish language
    "lo",  # 4182952	-- Lao language
    "la",  # 4181534	-- Latin language
    "lv",  # 4180064	-- Latvian language
    "lt",  # 4175772	-- Lithuanian language
    "lb",  # 46285414	-- Main spoken language Luxembourgish
    "mk",  # 4175905	-- Macedonian language
    "mg",  # 4180215	-- Malagasy language
    "ms",  # 4183666	-- Malay language
    "ml",  # 4180058	-- Malayalam language
    "mt",  # 4175735	-- Maltese language
    "mi",  # 4180223	-- Maori language
    "mr",  # 4182519	-- Marathi language
    "mn",  # 4182357	-- Mongolian language
    "my",  # 4181727	-- Burmese language
    "ne",  # 4175908	-- Nepali language
    "no",  # 4181530	-- Norwegian language
    "fa",  # 4183659	-- Persian language
    "pl",  # 4182515	-- Polish language
    "pt",  # 4181536	-- Portuguese language
    "pa",  # 4059175	-- Punjabi language
    "ro",  # 4181537	-- Rumanian language
    "ru",  # 4181539	-- Russian language
    "sm",  # 4181558	-- Samoan language
    "gd",  # 4181529	-- Scottish Gaelic language
    "sr",  # 40483799	-- Serbian language
    "sn",  # 4175936	-- Shona language
    "sd",  # 4181545	-- Sindhi language
    "si",  # 4183657	-- Sinhalese language
    "sk",  # 4181540	-- Slovak language
    "sl",  # 4182513	-- Slovenian language
    "so",  # 4182350	-- Somali language
    "es",  # 4182511	-- Spanish language
    "su",  # 4181554	-- Sundanese language
    "sw",  # 4181698	-- Swahili language
    "sv",  # 4175777	-- Swedish language
    "tg",  # 4181547	-- Tajik language
    "ta",  # 4181521	-- Tamil language
    "te",  # 4175767	-- Telugu language
    "th",  # 4183934	-- Thai language
    "tr",  # 4175744	-- Turkish language
    "uk",  # 4175904	-- Ukrainian language
    "ur",  # 4059788	-- Urdu language
    "uz",  # 4175743	-- Uzbek language
    "vi",  # 4181526	-- Vietnamese language
    "cy",  # 4175774	-- Welsh language
    "xh",  # 4180355	-- Xhosa language
    "yi",  # 4181531	-- Yiddish language
    "yo",  # 4183798	-- Yoruba language
    "zu",  # 4181699	-- Zulu language
    "he",
]  # 4180047	-- Hebrew language]
