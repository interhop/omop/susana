from sqlalchemy import Column, Integer, Text

from asynchron.db import Base, session


class RelationshipModel(Base):
    __tablename__ = "relationship"
    __table_args__ = {"schema": "susana"}

    relationship_id = Column(
        Text(),
        primary_key=True,
    )
    relationship_name = Column(Text())
    is_hierarchical = Column(Text())
    defines_ancestry = Column(Text())
    reverse_relationship_id = Column(Text())
    relationship_concept_id = Column(Integer)

    @classmethod
    def find_by_id(cls, relationship_id):
        return session.query(cls).filter_by(relationship_id=relationship_id).first()
