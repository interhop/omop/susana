# Development

## Releases

Dependencies needed:
- pip3 install -r requirements.txt (use a virtualenv)
- bump2version
- release-it

```bash
export GITLAB_TOKEN=<your token created on framagit>
make release.patch
```
## Development tools

```
python3 -m venv <path-to-the-venv-to-create>
source <path-to-the-venv-to-create>/bin/activate
pip3 install -r requirements.txt
```


### Towncrier

```
towncrier create <issue-number>.<issue-type>
# where issue type is feature, fix...
```


### Pre-commit

```
pre-commit install
```
