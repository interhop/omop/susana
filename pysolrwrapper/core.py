# for cls instance return
from __future__ import annotations

import logging

import numpy as np
from pysolr import Solr
from requests.auth import HTTPBasicAuth

from pysolrwrapper import utils
from pysolrwrapper.filter import Filter
from pysolrwrapper.loader_solr.solr_utils import SolrConf


class SolrQuery:
    def __init__(self, host: str, port: int, core: str, auth: HTTPBasicAuth = None):
        self.solr = Solr(
            f"http://{host}:{port}/solr/{core}",
            always_commit=True,
            results_cls=dict,
            auth=auth,
        )

        self._filter = []
        self._where = []
        self._query = None
        self._page = 1
        self._sort = []
        self._conf = {
            "defType": "edismax",
            "fl": "*",
            "sort": "score desc",
            "hl": "false",
            "hl.method": "unified",
            "termVectors": "true",
            "hl.fragsize": 10,
            "hl.snippets": 10,
            "hl.requireFieldMatch": "true",
            "hl.fl": "concept_name*",
            "sow": "false",
            "rows": 15,
            "start": 0,
            "wt": "python",
            "facet": "false",
            "facet.field": ["domain_id", "standard_concept"],
            "facet.limit": 5,
            "hl.tag.pre": "<strong>",
            "hl.tag.post": "</strong>",
        }

    @classmethod
    def from_conf(cls, solr_conf: SolrConf) -> SolrQuery:
        auth = HTTPBasicAuth(username=solr_conf.user, password=solr_conf.password)
        return cls(
            host=solr_conf.host,
            port=solr_conf.port,
            core=solr_conf.core,
            auth=auth,
        )

    def set_is_mapped(self, id: str, is_standard: bool = False):
        result = self.solr.search(q=f"id:{id}")["response"]["docs"]
        if not result:
            raise Exception("%s does not exist", id)
        project, current_is_mapped = result[0]["is_mapped"].split(" - ")
        value = (
            project + " - " + ("S" if is_standard or current_is_mapped == "S" else "NS")
        )
        doc = {"id": id, "is_mapped": value, "local_map_number": 1}
        self.solr.add([doc], fieldUpdates={"is_mapped": "set", "local_map_number": "inc"})
        return self

    def unset_is_mapped(self, id: str):
        result = self.solr.search(q=f"id:{id}")["response"]["docs"]
        if not result:
            raise Exception("%s does not exist", id)
        local_map_number = result[0]["local_map_number"]
        project, current_is_mapped = result[0]["is_mapped"].split(" - ")
        # this is an approximation that won't work in some scenario:
        # - when the removed mapping is S and there is only NS remaining we
        # will keep S by error
        # However the async process will modify to correct value
        new_is_mapped = (
            project + " - " + ("EMPTY" if local_map_number <= 1 else current_is_mapped)
        )
        doc = {"id": id, "local_map_number": -1, "is_mapped": new_is_mapped}
        self.solr.add([doc], fieldUpdates={"local_map_number": "inc", "is_mapped": "set"})
        return self

    def query(self, query: [str]):
        self._filter = query
        return self

    def algo(self, algo: {}):
        for key in algo:
            self._conf[key] = algo[key]
        return self

    def boost(self, algo: str):
        self._conf["boost"] = algo
        return self

    def weight(self, algo: str):
        self._conf["qf"] = algo
        return self

    def select(self, columns: [str]):
        if "id" not in columns:
            columns.append("id")
        self._conf["fl"] = ",".join(columns)
        return self

    def sort(self, attr: str, order: str):
        self._sort.append(f"{attr} {order}")
        return self

    def page(self, num: int):
        self._page = num - 1
        return self

    def limit(self, num: int):
        self._conf["rows"] = num
        return self

    def facet(self, columns: [str], limit=-1, missing=True, matches=[]):
        self._conf["facet"] = "true"
        self._conf["facet.field"] = columns
        self._conf["facet.limit"] = limit
        if len(matches) > 0:
            self._conf["facet.matches"] = "|".join(str(x) for x in matches)
        if missing:
            self._conf["facet.missing"] = "true"
        if limit == -1:
            self._conf.pop("facet.limit", None)
        return self

    def highlight(self, columns: [str]):
        self._conf["hl"] = "true"
        self._conf["hl.fl"] = ",".join(columns)
        return self

    def set_type_simple(self):
        self._conf["defType"] = "simple"
        return self

    def set_type_edismax(self):
        self._conf["defType"] = "edismax"
        return self

    def add_filter(self, value: [Filter]):
        self._filter.append(str(value))
        return self

    def add_where(self, value: [Filter]):
        self._where.append(str(value))
        return self

    def run(self) -> dict:
        if len(self._filter) > 0:
            self._query = " AND ".join(self._filter)
        else:
            self._query = "*:*"
        self._conf["fq"] = self._where
        self._conf["start"] = (self._page) * self._conf["rows"]
        # if self._conf['start'] != 0:
        #     self._conf["facet"] = 'false'
        if len(self._sort) > 0:
            self._conf["sort"] = ", ".join(self._sort)
        results = self.solr.search(q=self._query, **self._conf)
        logging.debug(str(self._conf))
        logging.debug(str(self._query))
        res = utils.convert_result(results)
        res["page_shown"] = self._page
        res["page_found"] = int(np.ceil(res["num_found"] / self._conf["rows"]))
        return res
