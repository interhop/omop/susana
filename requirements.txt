black~=21.6b0
selenium==3.141.0
pytest==6.2.3
testcontainers[postgresql]~=3.4.0
bump2version==1.0.1
towncrier~=21.3.0
tox~=3.24.0
click~=8.0.1
