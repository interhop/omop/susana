-- name: insert_concept_relationship!
with ins as (
insert into concept_relationship (
  concept_id_1      ,
  concept_id_2      ,
  relationship_id   ,
  valid_start_date  ,
  valid_end_date  ,
  invalid_reason
)
select
  t.concept_id_1	   ,
  t.concept_id_2	   ,
  t.relationship_id    ,
  t.valid_start_date   ,
  t.valid_end_date     ,
  t.invalid_reason
from tmp_concept_relationship as t
left join concept_relationship v on (
v.concept_id_1 = t.concept_id_1 and
v.concept_id_2 = t.concept_id_2 and
v.relationship_id = t.relationship_id
)
where v.concept_id_1 is null
returning concept_id_1, concept_id_2
)
insert into tmp_job (concept_id)
select concept_id_1 from ins
union all
select concept_id_2 from ins;

-- name: update_concept_relationship!
with upd as (
update concept_relationship set
concept_id_2    = v.concept_id_2		,
relationship_id = v.relationship_id     ,
valid_start_date = v.valid_start_date   ,
valid_end_date = v.valid_end_date   ,
invalid_reason = v.invalid_reason
from tmp_concept_relationship v
where (concept_relationship.concept_id_1 = v.concept_id_1 and
concept_relationship.concept_id_2 = v.concept_id_2        and
concept_relationship.relationship_id = v.relationship_id
)
and (
concept_relationship.valid_start_date is distinct from v.valid_start_date or
concept_relationship.valid_end_date is distinct from v.valid_end_date     or
concept_relationship.invalid_reason is distinct from v.invalid_reason
)
returning concept_relationship.concept_id_1, concept_relationship.concept_id_2
)
insert into tmp_job (concept_id)
select concept_id_1 from upd
union all
select concept_id_2 from upd;
