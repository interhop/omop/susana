import logging

import psycopg2
import pytest
import requests
from requests.auth import HTTPBasicAuth
from testcontainers.core.container import DockerContainer
from testcontainers.core.waiting_utils import wait_for
from testcontainers.postgres import PostgresContainer

from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient, rename
from pysolrwrapper.loader_solr.solr_utils import SolrClient, SolrConf
from pysolrwrapper.utils import resource_path_root

logging.getLogger().setLevel(level=logging.DEBUG)

SOLR_DATA = "/opt/bitnami/solr/data/"
SOLR_CONF = "/opt/bitnami/solr/conf"
SOLR_CORE = "susana_core"
SOLR_USER = "user"
SOLR_PASSWORD = "password"


@pytest.fixture(scope="session")
def solr_server():
    with DockerContainer("bitnami/solr:latest").with_bind_ports(8983, 8983).with_env(
        "SOLR_CORE", SOLR_CORE
    ).with_env("SOLR_ENABLE_CLOUD_MODE", "no").with_env(
        "SOLR_CORE_CONF_DIR", SOLR_CONF
    ).with_env(
        "SOLR_ENABLE_AUTHENTICATION", "yes"
    ).with_env(
        "SOLR_ADMIN_USERNAME", SOLR_USER
    ).with_env(
        "SOLR_ADMIN_PASSWORD", SOLR_PASSWORD
    ).with_exposed_ports(
        8983
    ).with_volume_mapping(
        host=str(resource_path_root() / "solr" / "conf"), container=SOLR_CONF
    ).with_volume_mapping(
        host=str(resource_path_root() / "data"), container=SOLR_DATA
    ) as solr:
        port = solr.get_exposed_port(8983)
        host = solr.get_container_host_ip()
        auth = HTTPBasicAuth(SOLR_USER, SOLR_PASSWORD)

        def connect():
            """
            hack to look after core ready
            """
            url = f"http://{host}:{port}/solr/{SOLR_CORE}/update"
            sess = requests.sessions.session()
            sess.auth = auth
            res = sess.get(url)
            if res.status_code != 400:
                raise Exception
            return

        wait_for(connect)
        yield solr


@pytest.fixture
def solr(solr_server):
    """For each test function, we load susana schema and purge it after"""
    port = solr_server.get_exposed_port(8983)
    host = solr_server.get_container_host_ip()
    solr_config = SolrConf(
        host=host, port=port, user=SOLR_USER, password=SOLR_PASSWORD, core=SOLR_CORE
    )
    solrClient = SolrClient(solr_config)
    yield solr_server
    solrClient.solr_delete_core(solr_core=SOLR_CORE)
    solrClient.solr_create_core(solr_core=SOLR_CORE)


@pytest.fixture
def solr_client(solr_server):
    port = solr_server.get_exposed_port(8983)
    host = solr_server.get_container_host_ip()
    solr_config = SolrConf(
        host=host, port=port, user=SOLR_USER, password=SOLR_PASSWORD, core=SOLR_CORE
    )
    solrClient = SolrClient(solr_config)
    yield solrClient


@pytest.fixture(scope="session")
def pgcon():
    with PostgresContainer("postgres:12.5") as postgres:
        port = postgres.get_exposed_port(5432)
        with psycopg2.connect(
            database="test", user="test", password="test", port=port, host="localhost"
        ) as conn:
            conn.set_session(autocommit=True)
            yield conn


@pytest.fixture
def susana_con(pgcon):
    """For each test function, we load susana schema and purge it after"""
    dump = str((resource_path_root() / "postgresql" / "dump.sql").read_text())
    with pgcon.cursor() as curs:
        curs.execute(dump)
        curs.execute("set search_path to susana")
        yield pgcon
        curs.execute("drop schema susana cascade")


@pytest.fixture
def susana_sample_con(susana_con):
    pg_client = PostgresClient(susana_con)
    csvs = (resource_path_root() / "postgresql").glob("*csv")
    sorted_csvs = sorted(csvs)
    list(
        map(lambda csv: pg_client.bulk_load(rename(str(csv.stem)), str(csv)), sorted_csvs)
    )
    yield susana_con
