import socket
import time

from selenium.webdriver import DesiredCapabilities
from testcontainers.selenium import BrowserWebDriverContainer


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(("10.255.255.255", 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = "127.0.0.1"
    finally:
        s.close()
    return IP


def test_mapping(containers, path_root):
    with BrowserWebDriverContainer(DesiredCapabilities.CHROME) as chrome:
        webdriver = chrome.get_driver()
        # selenium run inside docker so localhost is not reachable
        # -> get the private ip instead
        host_ip = get_ip()
        webdriver.get(f"http://{host_ip}")
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo00.png"))
        webdriver.find_element_by_css_selector("#registrationSearch-img").click()
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo01.png"))
        webdriver.find_element_by_css_selector("#__input0-inner").send_keys("admin")
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo02.png"))
        webdriver.find_element_by_css_selector("#__input1-inner").send_keys("toto")
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo03.png"))
        webdriver.find_element_by_css_selector("#__button26-BDI-content").click()
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo04.png"))
        webdriver.find_element_by_css_selector("#searchField-I").send_keys("radio*")
        webdriver.find_element_by_css_selector("#searchField-search").click()
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo05.png"))
        num_found = webdriver.find_element_by_css_selector("#numFound-bdi").text
        assert int(num_found) == 4
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo06.png"))
        webdriver.find_element_by_css_selector("#__box0-oTable-0-CbBg").click()
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo07.png"))
        webdriver.find_element_by_css_selector("#mapper-BDI-content").click()
        time.sleep(2)  # this does not work https://stackoverflow.com/a/26567563/3865083
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo08.png"))
        webdriver.find_element_by_css_selector(
            "#researchIconTabFilterMapperPage-icon"
        ).click()
        time.sleep(2)
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo09.png"))
        webdriver.find_element_by_css_selector("#searchFieldMapper-search").click()
        time.sleep(2)
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo10.png"))
        webdriver.find_element_by_css_selector("#__text64-__clone0").click()
        time.sleep(2)
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo11.png"))
        webdriver.find_element_by_css_selector("#__button28-BDI-content").click()
        webdriver.get_screenshot_as_file(str(path_root / "tmp/foo12.png"))
