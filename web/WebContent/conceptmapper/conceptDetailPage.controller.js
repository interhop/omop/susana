
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/conceptAjax",
	"./utils/conceptRelationshipAjax",
	"./utils/userAjax",
	"./utils/keyboardEvents",

		], function(Controller, conceptAjax, conceptRelationshipAjax, userAjax, keyboardEvents) {
		   "use strict";

return Controller.extend("conceptmapper.conceptDetailPage", {	


	
	goToConceptDetail : function(oEvt, iIdx) {
		var sVal = oEvt.getSource().getAggregation('cells')[iIdx].getProperty('text');
		conceptAjax.getConceptDetailAjax(sVal);
	},

	moreInfos : function(oEvt) {
		
		
		concept_id_2 = sap.ui.getCore().byId(oEvt.getParameters('text').id).getParent().getAggregation('cells')[3].getProperty('text')
		var relations = sap.ui.getCore().getModel('conceptDetailModel').oData.relations
		
		for (var i in relations) {
			if (relations[i].concept_id === parseInt(concept_id_2)) {
				var m_mappping_rate = relations[i].m_mapping_rate;
				var m_mapping_comment = relations[i].m_mapping_comment;
				break;
			}
		}
		if (m_mapping_comment === "") {
				m_mapping_comment = "No Mapping Comment";
		}
		
		
		var moreInfosDialog = new sap.m.Dialog({
			title : "More infos",
			icon : "sap-icon://doc-attachment",
			content: [
				new sap.m.RatingIndicator({		
					value: relations[i].m_mapping_rate,
					enabled: false,
				}),
				new sap.m.TextArea({
					placeholder: "no mapping comment!",
					width: "100%",
					value:m_mapping_comment,
					enabled: false,
				})
				
			]
		});

		
		moreInfosDialog.addButton(
				new sap.m.Button({
					text: "Reject",
					icon: "sap-icon://decline",
					press: function () {
						moreInfosDialog.close();
					}
				})
        );
		
		moreInfosDialog.open();
		
		
		
		
		
	},
	
	modifRelation: function(oEvt) {
		
		
//		conceptsDetailModel
		var concept = sap.ui.getCore().getModel('conceptDetailModel').oData;
		var oData = {};
	
		oData.concept_id_2 = sap.ui.getCore().byId(oEvt.getParameters('text').id).getParent().getAggregation('cells')[3].getProperty('text');
		oData.relationship_id = 'Maps to';
		
		
		var relations = sap.ui.getCore().getModel('conceptDetailModel').oData.relations
		
		for (var i in relations) {
			if (relations[i].concept_id === parseInt(oData.concept_id_2)) {
				var action = relations[i].action;
			}
		}
		
		
		
		
		if (action === 'delete') {

				
			var odeleteRelationDialog = new sap.m.Dialog({
				title : "Mapper",
				icon : "sap-icon://doc-attachment"
			});
			
			odeleteRelationDialog.addContent(new sap.m.Text({text: "You will delete relation. Are you sure?"}));
	
			odeleteRelationDialog.addButton(
	                new sap.m.Button({
	                    text: "Yes",
						type: "Accept",
						icon: "sap-icon://accept",
						
	                    press: function () {
	                    	
	                    	conceptRelationshipAjax.deleteConceptRelationshipAjax(concept.concept_id, oData);
	                    	
	            			odeleteRelationDialog.close();
	                    	
	            			conceptAjax.getConceptDetailAjax(concept.concept_id);
	                    	
	
	                  
	                    }
	
	                })
	        );
			
			
			odeleteRelationDialog.addButton(
					new sap.m.Button({
						text: "Reject",
						icon: "sap-icon://decline",
						press: function () {
							odeleteRelationDialog.close();
						}
					})
	        );
			
			odeleteRelationDialog.open();
			
			
		} else if (action === 'undo') {
			
			var oMapperDialog = new sap.m.Dialog({
				title : "Mapper",
				icon : "sap-icon://doc-attachment"
			});
		    $(document).unbind('keydown');

			oMapperDialog.addContent(new sap.m.Text({text: "You will undo relation deletion. Are you sure?"}));

			oMapperDialog.addButton(
	                new sap.m.Button({
	                    text: "Yes",
						type: "Accept",
						icon: "sap-icon://accept",
						
	                    press: function () {
	                    	
	                  
	     	                    	
	                    	conceptRelationshipAjax.putConceptRelationshipAjax(concept.concept_id, oData);
	     	                    	
	                    	oMapperDialog.close();
	     	                    	
	     	                conceptAjax.getConceptDetailAjax(concept.concept_id);
	     	                    	
	     	
	     	                  
	                    }
	                })
	                    	
	                    	
	                    	

	        );
			
			
			oMapperDialog.addButton(
					new sap.m.Button({
						text: "Reject",
						icon: "sap-icon://decline",
						press: function () {
							oMapperDialog.close();
							
						}
					})
	        );
			
			oMapperDialog.open();
			
		
		}
		
		
		
		
		

		
		
	},
	appBack : function (oEvt) {
		keyboardEvents.searchPage();

		app.back();
		
	},
	userInfos : function (oEvt) {
		userAjax.getUser(sap.ui.getCore().getModel('loginModel').getProperty('/m_username'))
		app.to('idUserInfosPage');
	}, 
	
	logout : function (oEvt) {
			sap.ui.controller("conceptmapper.searchPage").logout();
			app.to('idSearchPage');
	},
	
	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 * 
	 * @memberOf conceptmapper.conceptDetailPage
	 */
//	onInit : function(oEvt) {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's
 * View is re-rendered (NOT before the first rendering! onInit() is used for
 * that one!).
 * 
 * @memberOf conceptmapper.conceptDetailPage
 */

/**
 * Called when the View has been rendered (so its HTML is part of the document).
 * Post-rendering manipulations of the HTML could be done here. This hook is the
 * same one that SAPUI5 controls get after being rendered.
 * 
 * @memberOf conceptmapper.conceptDetailPage
 */
// onAfterRendering: function() {
//
//
// },
/**
 * Called when the Controller is destroyed. Use this one to free resources and
 * finalize activities.
 * 
 * @memberOf conceptmapper.conceptDetailPage
 */
// onExit: function() {
//
// }
});
});