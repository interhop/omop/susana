sap.ui.jsview("conceptmapper.mapperPage", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf conceptmapper.mapperPage
	*/ 
	getControllerName : function() {
		return "conceptmapper.mapperPage";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf conceptmapper.mapperPage
	*/ 
	createContent : function(oController) {

		/////////// FILTER TABS //////////////////////
		
		var oItemTemplateStandard = new sap.ui.core.Item({
			key : "{standard_concept_mapper>key}",
			text : "{standard_concept_mapper>text}",
		//	enabled : "{standard_concept>enabled}"
		});
		var oStandardMCB = new sap.m.MultiComboBox({
			id : "oStandardMapperMCB",
			 width : "100%",
			placeholder : "{i18n>standard_label}",
			items : {
				path : "standard_concept_mapper>/items",
				template : oItemTemplateStandard
			},
			selectedKeys : "{searchFieldMapperModel>/standard_concept}",
			selectionFinish: [ oController.searcher, oController ],

		}).addStyleClass('researchMapperPage');
		
		
		var oItemTemplateDomain = new sap.ui.core.Item({
			key : "{domain_id_mapper>key}",
			text : "{domain_id_mapper>text}",
//			enabled : "{domain_id_mapper>enabled}"
		});
		var oDomainMCB = new sap.m.MultiComboBox({
			id : "oDomainMapperMCB",
			placeholder : "{i18n>domain_label}",
			 width : "100%",

			items : {
				path : "domain_id_mapper>/items",
				template : oItemTemplateDomain
			},
			selectedKeys : "{searchFieldMapperModel>/domain_id}",
			selectionFinish: [ oController.searcher, oController ],

		});

		var oItemTemplateVoc = new sap.ui.core.Item({
			key : "{vocabulary_id_mapper>key}",
			text : "{vocabulary_id_mapper>text}",
//			enabled : "{vocabulary_id_mapper>enabled}"
		});
		var oVocMCB = new sap.m.MultiComboBox({
			id : "oVocMapperMCB",
			 width : "100%",

			placeholder : "{i18n>vocabulary_label}",
			items : {
				path : "vocabulary_id_mapper>/items",
				template : oItemTemplateVoc
			},
			selectedKeys : "{searchFieldMapperModel>/vocabulary_id}",
//			{
//				path : "vocabulary_id_mapper>/selected",
//				template : "{vocabulary_id_mapper>selected}"
//			},
			selectionFinish: [ oController.searcher, oController ],

		});
		var oEnglishButton = new sap.m.ToggleButton({
        	id: "oEnglishButton",
        	text: "English",
        	pressed: true,

			press : [ "next", oController.englishButton, oController ],
    	});
		var oIntralanguageButton = new sap.m.ToggleButton({
        	id: "oIntralanguageButton",
        	text: "Intra-language",
        	pressed: false,

			press : [ "next", oController.intralanguageButton, oController ],
    	});
		
		
		
		
		var oSearchFieldMapper = new sap.m.SearchField({
			
			id: 'searchFieldMapper',
			value : "{searchFieldMapperModel>/search_string}",		        			
			search : [ oController.searcher, oController ],
		}).addStyleClass('searchField');
		
		var oFlexBoxFilter = new sap.m.FlexBox({
			renderType: "Bare",
			alignItems: "Center",
			
			justifyContent: "Center",
			items: [ oStandardMCB, oDomainMCB, oVocMCB ]
			
		});
		
//		
		var oLanguageChoose = new sap.m.FlexBox({
			renderType: "Bare",
			alignItems: "Stretch",

			justifyContent: "SpaceAround",
			items: [ oEnglishButton, oIntralanguageButton ]
			
		});
		
		var oFlexBoxFilterall = new sap.m.FlexBox({
			renderType: "Div",
			alignItems: "Stretch",
			height: "300%",

			justifyContent: "Center",
			items: [ oLanguageChoose, oFlexBoxFilter]
			
		});
		
		//////////////////  IconTabBar ////////////////////
		var oIcTBMapper = new sap.m.IconTabBar("oIcTBMapper", {
			expanded: false,
			items: [

				new sap.m.IconTabFilter({
					text: "{i18n>selected_concepts}",
					showAll : true,
					count : "{conceptsDetailModel>/to_map_number}",
					enabled:false,

				}),
				
				new sap.m.IconTabSeparator(),
				
				
				new sap.m.IconTabFilter({
					iconColor: sap.ui.core.IconColor.Critical,
					id: "researchIconTabFilterMapperPage",
					text: "{i18n>research}",
					icon: "sap-icon://search",

					content: [ 

						
						oFlexBoxFilterall,

						oSearchFieldMapper
					]
				
				}),
			
				
				
				new sap.m.IconTabFilter({
					iconColor: sap.ui.core.IconColor.Critical,
					text: "{i18n>algos}",
					icon: "sap-icon://developer-settings",
					//enabled:false,

					content: [
						
						
	                	new sap.m.ToggleButton({
	                    	id: "algo1Toggle", 
	                    	pressed: true,
	                    	text: "Simple", 
	    					icon : "sap-icon://radar-chart",
	    					press : [ oController.algo_one, oController ],

	                	}),

	                	new sap.m.ToggleButton({
	                    	id: "algo2Toggle", 
	                    	pressed: false,
	                    	text: "Use frequency", 
	    					icon : "sap-icon://radar-chart",
	    					press : [ oController.algo_two, oController ],

	                	}),
	                	
//	                	new sap.m.ToggleButton({
//	                    	id: "algo3Toggle", 
//	                    	pressed: false,
//	                    	text: "Use value", 
//	    					icon : "sap-icon://radar-chart",
//	    					press : [ oController.algo_three, oController ],
//
//	                	}),
	                	

					]
				}),


			]
		});
				
		

		////////// Concepts details /////////////////

		var aColumns = [
			new sap.m.Column({
				header : new sap.m.Label({
					text: "{i18n>concept_id}",

				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text: "{i18n>concept_name}",
				}),
				width : '60%',

			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text: "{i18n>concept_code}",
				})
			}),
			new sap.m.Column({
				header : new sap.m.Label({
					text: "{i18n>vocabulary_id}",
				})
			}),
			new sap.m.Column({
				header : new sap.m.Text({
					text: "{i18n>domain_id}",
				}),
			}),
			new sap.m.Column({
				header : new sap.m.Text({
					text: "Mapping",
				}),
			}),
		];
		
		var oTemplate = new sap.m.ColumnListItem({
			highlight:"{conceptsDetailModel>status}",
			cells : [
				new sap.m.Link({
					text : "{conceptsDetailModel>concept_id}",
					wrapping : false,
                	press: [  oController.goToConceptDetail, oController ],

				}),
				new sap.m.Text({
					text : "{conceptsDetailModel>concept_name}",
					wrapping : true
				}),
				new sap.m.Text({
					text : "{conceptsDetailModel>concept_code}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsDetailModel>vocabulary_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsDetailModel>domain_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsDetailModel>is_mapped}",
					wrapping : false
				}),
			]
		});
		
		
		var oConceptDetailPanelContent = new sap.m.Table({
			fixedLayout:false,
			columns : aColumns,

		});

		
		
		
		
		oConceptDetailPanelContent.bindItems({
			path: "conceptsDetailModel>/to_map",
			template : oTemplate,
			key: "id"
		});
		
		
		
		
		
		
		
		
		////////////// Relations Tabs ///////////////
		var aColumns = [
			new sap.m.Column({
				id: "conceptIdMapper",

				header : new sap.m.Label({
					text: "{i18n>concept_id}",
				})
			}),
			new sap.m.Column({
				id: "conceptNameMapper",

				header : new sap.m.Label({
					text: "{i18n>concept_name}",
				}),
				width : '60%',

			}),
			
			new sap.m.Column({
				id: "conceptClassMapper",
				header : new sap.m.Label({
					text : "{i18n>concept_class_id}"
				}),
				visible: false,

			}),
			
			
			new sap.m.Column({
				id: "conceptCodeMapper",

				header : new sap.m.Label({
					text: "{i18n>concept_code}",
				})
			}),
			new sap.m.Column({
				id: "vocabularyMapper",

				header : new sap.m.Label({
					text: "{i18n>vocabulary_id}",
				})
			}),
			new sap.m.Column({
				id: "domainMapper",

				header : new sap.m.Text({
					text: "{i18n>domain_id}",
				}),
			}),
			new sap.m.Column({
				id: "standardMapper",

				header : new sap.m.Label({
					text: "{i18n>standard_concept}",
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "frequencyMapper",

				header : new sap.m.Label({
					text: "{i18n>frequency}",
				}),
				visible: false,

			}),
			new sap.m.Column({
				id: "scoreMapper",

				header : new sap.m.Label({
					text: "{i18n>score}",
				}),
			}),
			

		];
		


		
		
		
		
		var oTemplate = new sap.m.ColumnListItem({
			type: "Active",
			press: [ "clickOnRelationTable", oController.addRelation, oController ],
			cells : [
				new sap.m.Link({
					text : "{conceptsMapperModel>concept_id}",
					wrapping : false,
                	press: [  oController.goToConceptDetail, oController ],

				}),
				new sap.m.Text({
					text : "{conceptsMapperModel>concept_name}",
					wrapping : true
				}),
				
				
				new sap.m.Text({
					text : "{conceptsMapperModel>concept_class_id}",
					wrapping : false,
				}),
				
				
				
				new sap.m.Text({
					text : "{conceptsMapperModel>concept_code}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsMapperModel>vocabulary_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsMapperModel>domain_id}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsMapperModel>standard_concept}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsMapperModel>frequency}",
					wrapping : false
				}),
				new sap.m.Text({
					text : "{conceptsMapperModel>score}",
					wrapping : false
				}),
				
				
			]
		});

		var oTable = new sap.m.Table("relationTable", {
			growing: true,
			fixedLayout:false,

			growingThreshold: 10,
			columns : aColumns,

		});

		
		
		
		oTable.bindItems({
			path: "conceptsMapperModel>/docs",
			template : oTemplate,
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		var oConceptDetailHeader = new sap.m.Toolbar({
			content : [
				new sap.m.Title({
					text : "{i18n>concepts_detail}"
				}),

				
				
				new sap.m.ToolbarSpacer(),
				
				
				new sap.m.Button({
            		id : "oButtonPrevConceptToMap",
                	text: "{i18n>previous}",
					type : "Transparent",
					visible: false,
					enabled:false,
					icon : "sap-icon://navigation-left-arrow",
					press : [ "prev", oController.newConcept, oController ],
            		
            	}),  
            	new sap.m.Button({
            		id : "oButtonNextConceptToMap",
                	text: "{i18n>next}",
					type : "Transparent",
					visible: false,
					enabled:false,

					iconFirst: false,
					icon : "sap-icon://navigation-right-arrow",
					press : [ "next", oController.newConcept, oController ],
            	}),
				
				
				
				]
			});

		
		var oConceptDetailPanel = new sap.m.Panel("oHeaderMapperPanel", {
			expandable : true,
			expanded : true,
			headerToolbar: oConceptDetailHeader,
			content : [ oConceptDetailPanelContent

			]
		});
		
		var oTableHeader = new sap.m.Toolbar({
		content : [
			new sap.m.Title({
				text : "{i18n>choose_relation}"
			}),

			
			new sap.m.ToolbarSpacer(),
			new sap.m.Button({
//        		id : "oButtonNextConceptToMap",
            	text: "{i18n>no_matching_concept}",
//				type : "Transparent",
				type : "Reject",

//				visible: false,
//				enabled:false,
//
//				iconFirst: false,
//				icon : "sap-icon://navigation-right-arrow",
				press : [ "0", oController.addRelation, oController ],
        	}),
        	
			new sap.m.ToolbarSpacer(),
			new sap.m.ToggleButton({
				id:"expertMode",
				text:"{i18n>quick_mode}",
            	tooltip: "{i18n>quick_mode_tooltip}", 
				press : function(e) {
					if (e.getSource().getPressed()) {
						sap.ui.getCore().getModel('conceptsDetailModel').oData.expert_mode = true;
					} else {
						sap.ui.getCore().getModel('conceptsDetailModel').oData.expert_mode = false;
					}
				},
			}),
			new sap.m.ToggleButton({
				text:"{i18n>rate_comment}",
				pressed: true,
            	tooltip: "{i18n>rate_comment_tooltip}", 
				press: function(e) {
					if (e.getSource().getPressed()) {
						sap.ui.getCore().byId("oConceptMappingRate").setVisible(true);
						sap.ui.getCore().byId("oConceptMappingComment").setVisible(true);

					} else {
						sap.ui.getCore().byId("oConceptMappingRate").setVisible(false);
						sap.ui.getCore().byId("oConceptMappingComment").setVisible(false);
					}
					
				},
			}),
			new sap.m.Button({
            	id: "customizeMapperPage", 
				icon: "sap-icon://customize",
            	tooltip: "{i18n>select_columns}", 
				type: "Transparent",
				press: [oController.openCustomizeSPFragment, oController],
			}),

			]
		});
	
		var oContentPanel = new sap.m.Panel("oContentMapperPanel", {
			expandable : false,
			expanded : true,
			headerToolbar : oTableHeader, 
			content : [ 
				new sap.m.RatingIndicator("oConceptMappingRate", {

				}),
				new sap.m.TextArea("oConceptMappingComment",
					 {
						placeholder: "{i18n>mapping_comments}",
						width: "100%",
						
					}),
				oTable 
			]
		});

		

		
		
		
		
		var oSubHeader = new sap.m.Bar({
			contentMiddle : [ new sap.m.Label({
				text : "{i18n>mapping_tool}"
			}) ]
		});
		
		
		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			
			showNavButton : true,
			navButtonPress : [ oController.navBack, oController ], 

			headerContent : [ 
			
				new sap.m.Button({
					text: '{loginModel>/m_username}',
					icon : "sap-icon://employee",
					enabled:true, 
					press: [ oController.userInfos, oController ],

				}),
				new sap.m.Button({
					text: 'Log-out',
					icon : "sap-icon://decline",
					press: [ oController.logout, oController ],
				}) ],
			showSubHeader : true,
			subHeader : oSubHeader,
			content : [ oIcTBMapper, oConceptDetailPanel, oContentPanel
				 ]
		});

		
		

		
		
 		return oPage;
	}

});