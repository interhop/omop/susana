function setBasicResearchParamsReviewer() {
	var oData = {};
    var oRowsNumber = parseInt(sap.ui.getCore().byId('oRowInPageReviewerPage').getProperty('value'));
    
    if (!isNaN(oRowsNumber) && Number.isInteger(oRowsNumber)) {
        oData.limit = oRowsNumber;
    }
    
    
    
	
	var oProjectTypeMCB_ref = sap.ui.getCore().byId('oTypeProjectReviewer').getSelectedItems();
	var oProjectTypeMCB_list = [];
	for (var i in oProjectTypeMCB_ref) {
		oProjectTypeMCB_list.push(oProjectTypeMCB_ref[i].getProperty('key').toUpperCase());
	}
	
	oData.project_ids = oProjectTypeMCB_list;
    return oData;

}


function resetReviewerProperty() {
	sap.ui.getCore().byId('oConceptMappingComment').resetProperty('selectedKeys');


}




sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/conceptAjax",
	"./utils/conceptRelationshipsAjax",
	"./utils/voteAjax",
	"./customControls/hoverLink",
	"./utils/keyboardEvents",


		], function(Controller, conceptAjax, conceptRelationshipsAjax, voteAjax, hoverLink, keyboardEvents) {
		   "use strict";

return Controller.extend("conceptmapper.reviewerPage", {	
	
	
	searcherGoToPage : function() {
		
		var oData = setBasicResearchParamsReviewer();
		
        oData.page_shown = parseInt(sap.ui.getCore().byId('oSearchFielGoToPageReviewer').getProperty('value')) - 1 ;
        
        if (oData.page_shown < 0 || !oData.page_shown) {
        	oData.page_shown = 0;
        }
		
        conceptRelationshipsAjax.getConceptRelationshipsAjax(oData);

		var oModel = sap.ui.getCore().getModel('relationshipsModel');
        var aPageFound  = oModel.getProperty("/page_found");
        var aPageShown  = oModel.getProperty("/page_shown");
        if (aPageFound === 0 || aPageFound === 1) {
        	sap.ui.getCore().byId('oButtonPrevReviewer').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNextReviewer').setEnabled(false);
        } else {
        	sap.ui.getCore().byId('oButtonPrevReviewer').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNextReviewer').setEnabled(true);
        }		
        
        var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + ++aPageShown;
        var buttonTextFound = sap.ui.getCore().getModel('i18n').getProperty('total_pages') + ' : ' + aPageFound;
        
        sap.ui.getCore().byId('oButtonCurrentPageReviewer').setText(buttonTextShown);
        sap.ui.getCore().byId('oButtonTotalPagesReviewer').setText(buttonTextFound);
	}, 
	
	searchRelation : function() {
		
		var oData = setBasicResearchParamsReviewer();

		debugger;
		sap.ui.getCore().byId('oSearchFielGoToPageReviewer').resetProperty('value');
		
		conceptRelationshipsAjax.getConceptRelationshipsAjax(oData);

		var oModel = sap.ui.getCore().getModel('relationshipsModel');
        var aPageFound  = oModel.getProperty("/page_found");
        var aPageShown  = oModel.getProperty("/page_shown");
        if (aPageFound === 0 || aPageFound === 1) {
        	sap.ui.getCore().byId('oButtonPrevReviewer').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNextReviewer').setEnabled(false);
        } else {
        	sap.ui.getCore().byId('oButtonPrevReviewer').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNextReviewer').setEnabled(true);
        }		
        
        var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + ++aPageShown;
        var buttonTextFound = sap.ui.getCore().getModel('i18n').getProperty('total_pages') + ' : ' + aPageFound;
        
        sap.ui.getCore().byId('oButtonCurrentPageReviewer').setText(buttonTextShown);
        sap.ui.getCore().byId('oButtonTotalPagesReviewer').setText(buttonTextFound);
	}, 
	
	
	prevPage :  function (oEvt) {
				
		var oData = setBasicResearchParamsReviewer();
		sap.ui.getCore().byId('oSearchFielGoToPageReviewer').resetProperty('value');


		var oModel = sap.ui.getCore().getModel('relationshipsModel');
        var aPageShown  = oModel.getProperty("/page_shown");
        
        oData.page_shown = aPageShown - 1;
        
        if (oData.page_shown <= 0) {
        	sap.ui.getCore().byId('oButtonPrevReviewer').setEnabled(false);
        }
    	sap.ui.getCore().byId('oButtonNextReviewer').setEnabled(true);

    	conceptRelationshipsAjax.getConceptRelationshipsAjax(oData);
    	
		var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + ++oData.page_shown;
        
        sap.ui.getCore().byId('oButtonCurrentPageReviewer').setText(buttonTextShown);
        	
	},
	
	
	nextPage :  function (oEvt) {
		
		var oData = setBasicResearchParamsReviewer();
		sap.ui.getCore().byId('oSearchFielGoToPageReviewer').resetProperty('value');

		
		var oModel = sap.ui.getCore().getModel('relationshipsModel');
        var aPageShown  = oModel.getProperty("/page_shown");
        var aPageFound  = oModel.getProperty("/page_found");
        
        oData.page_shown = aPageShown + 1;
        
        if (oData.page_shown + 1 >= aPageFound) {
        	sap.ui.getCore().byId('oButtonNextReviewer').setEnabled(false);
        }
    	sap.ui.getCore().byId('oButtonPrevReviewer').setEnabled(true);

    	conceptRelationshipsAjax.getConceptRelationshipsAjax(oData);
        
        var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + ++oData.page_shown;
        
        sap.ui.getCore().byId('oButtonCurrentPageReviewer').setText(buttonTextShown);

		
		
	},
	
	
	vote : function(oEvt, sVote) {
		var oData = {};
		oData.table = 'concept_relationship';
		oData.value_as_string = sVote;
		var sConceptRelationshipId = oEvt.getSource().getParent().getAggregation('cells')[0].getProperty("text");
		debugger;
		if (sVote === 'UP') {
			//	if UP abled => putVote and disabled UP button,
			//     if DOWN disabled => able DOWN button + deleteVote DOWN
			//     vote + 1
		
			if (oEvt.getSource().getParent().getAggregation('cells')[11].getType() === "Transparent") { // UP enabled == true
				voteAjax.voteAjax(sConceptRelationshipId, oData, "PUT");
				oEvt.getSource().getParent().getAggregation('cells')[11].setType("Accept");
				var path = oEvt.getSource().getBindingContext('relationshipsModel').getPath();
				var vote = sap.ui.getCore().getModel('relationshipsModel').getProperty(path).vote + 1;
				if (oEvt.getSource().getParent().getAggregation('cells')[12].getType() === "Reject") { // DOWN enabled == false
					oEvt.getSource().getParent().getAggregation('cells')[12].setType("Transparent");
					oData.value_as_string = 'DOWN';
					voteAjax.voteAjax(sConceptRelationshipId, oData, "DELETE");
					vote = vote + 1;
				} 
				sap.ui.getCore().getModel('relationshipsModel').setProperty(path+"/vote",vote);
			}
			else if (oEvt.getSource().getParent().getAggregation('cells')[11].getType() === "Accept"){
				oEvt.getSource().getParent().getAggregation('cells')[11].setType("Transparent");
				oData.value_as_string = 'UP';
				voteAjax.voteAjax(sConceptRelationshipId, oData, "DELETE");
				var path = oEvt.getSource().getBindingContext('relationshipsModel').getPath();
				var vote = sap.ui.getCore().getModel('relationshipsModel').getProperty(path).vote - 1;
				sap.ui.getCore().getModel('relationshipsModel').setProperty(path+"/vote",vote);
				
			}

		} else if (sVote === 'DOWN') {
			if (oEvt.getSource().getParent().getAggregation('cells')[12].getType() === "Transparent") { // DOWN enabled == true
				voteAjax.voteAjax(sConceptRelationshipId, oData, "PUT");
				oEvt.getSource().getParent().getAggregation('cells')[12].setType("Reject");
				var path = oEvt.getSource().getBindingContext('relationshipsModel').getPath();
				var vote = sap.ui.getCore().getModel('relationshipsModel').getProperty(path).vote - 1;
				if (oEvt.getSource().getParent().getAggregation('cells')[11].getType() === "Accept"){ // UP enabled == false
					oEvt.getSource().getParent().getAggregation('cells')[11].setType("Transparent");
					oData.value_as_string = 'UP';
					voteAjax.voteAjax(sConceptRelationshipId, oData, "DELETE");
					vote = vote - 1;
				} 
				sap.ui.getCore().getModel('relationshipsModel').setProperty(path+"/vote",vote);
			}
			else if (oEvt.getSource().getParent().getAggregation('cells')[12].getType() === "Reject"){
				oEvt.getSource().getParent().getAggregation('cells')[12].setType("Transparent");
				oData.value_as_string = 'DOWN';
				voteAjax.voteAjax(sConceptRelationshipId, oData, "DELETE");
				var path = oEvt.getSource().getBindingContext('relationshipsModel').getPath();
				var vote = sap.ui.getCore().getModel('relationshipsModel').getProperty(path).vote + 1;
				sap.ui.getCore().getModel('relationshipsModel').setProperty(path+"/vote",vote);	
			}
		}
	},
	
	goToConceptDetail :  function (oEvt) {

        // GET AJAX call on concept
		conceptAjax.getConceptDetailAjax(oEvt.getSource().getProperty('text'));

		app.to('idConceptDetailPage', 'slide'); //('fade', 'flip', 'show', 'slide =default')
	},
	
	goToUserPage :  function (oEvt) {
		sap.ui.controller("conceptmapper.searchPage").goToUserPage();

	},
	
	userInfos :  function (oEvt) {
		sap.ui.controller("conceptmapper.searchPage").userInfos();

	},
	
	goToSearchPage : function() {
		keyboardEvents.searchPage();

		app.to('idSearchPage', 'flip');
	},
	
	logout : function (oEvt) {
		
		sap.ui.controller("conceptmapper.searchPage").logout();

		app.to('idSearchPage');
	
	},
	
	showDetailPopover : function(oEvt) {

  	  
			var oPopover = this._mViewSettingPopover['conceptmapper.conceptDetail'];
			if (!oPopover) {
				oPopover = sap.ui.jsfragment('conceptmapper.conceptDetail', this); //this = oController object
				this._mViewSettingPopover['conceptmapper.conceptDetail'] = oPopover;
			}
			
			
			conceptAjax.getConceptDetailAjax(oEvt.getSource().getProperty('text'));
	
			oPopover.openBy(oEvt.getSource());
	
	
	},

	hideDetailPopover : function(oEvt) {

	  
		var oPopover = this._mViewSettingPopover['conceptmapper.conceptDetail'];
		if (oPopover) {
			oPopover.close();
	
		}
	
	},

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf conceptmapper.reviewerPage
*/
	onInit: function() {
		
		this._mViewSettingPopover = {}; 
		sap.ui.getCore().byId("__container2-Master").toggleStyleClass("toggle-masterPage");
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf conceptmapper.reviewerPage
*/
//	onBeforeRendering: function() {
//		
//		sap.ui.controller("conceptmapper.reviewerPage").searchRelation();
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf conceptmapper.reviewerPage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf conceptmapper.reviewerPage
*/
//	onExit: function() {
//
//	}

});
});