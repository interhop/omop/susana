sap.ui.define([], function() {
   "use strict";

   return {
	   


	   getConceptDetailAjax : function (sVal) {
		   
	    	sap.ui.core.BusyIndicator.show(0);

	    	var URL_concept = "http://localhost:5000/concept/";
	   	
		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		   	}
		   	
		   	
		   	if (sVal !== undefined) {
		   		$.ajax({
		   		type:"GET",
		   		url: URL_concept + sVal,
		   		dataType:"json",
		   		async: false,
		   		header: {
		   			"Content-Type": "application/json",
		   		},
		   		success: function(data, response, xhr) {
		   			if (data.relations) {
		   				
		   				
		   				debugger;
		   				
		   				for (var i = 0; i < data.relations.length; i++) {
		   					var relation = data.relations[i];
		   					
		   					// DELETE actions
		   					if (Number.isInteger(relation.m_user_id) && relation.invalid_reason !== 'Deleted') {
		   						relation.erasableIcon = "sap-icon://delete";
		   						relation.notErasableText = sap.ui.getCore().getModel('i18n').getProperty('delete_relation');
		   						relation.typeErasableButton = "Reject";
		   						relation.action = "delete";
		   					} else if (Number.isInteger(relation.m_user_id) && relation.invalid_reason === 'Deleted') {
		   						relation.erasableIcon = "sap-icon://undo";
		   						relation.notErasableText = sap.ui.getCore().getModel('i18n').getProperty('enable_relation');
		   						relation.typeErasableButton = "Accept";
		   						relation.action = "undo";
		   					} else {
		   						relation.notErasableText = sap.ui.getCore().getModel('i18n').getProperty('no_deletion');
		   						relation.typeErasableButton = "Transparent";
		   						relation.enabledErasable = false;
		   					}
		   					
		   					// More infos actions
		   					if (relation.m_user_id && (relation.m_mapping_comment !== "" || relation.m_mapping_rate !== 0)) {
		   						relation.moreInfoIcon = "sap-icon://add";
		   						relation.typeMoreInfoButton = "Accept";
		   						relation.moreInfoText = "";
	
		   					} else {
		   						relation.moreInfoText = sap.ui.getCore().getModel('i18n').getProperty('no_info');
		   						relation.typeMoreInfoButton = "Transparent";
		   						relation.enabledMoreInfo = false;
	
		   					}
		   					
		   				}
		   				
		   			}
	
		   			var oModel = new sap.ui.model.json.JSONModel(data);
		   			sap.ui.getCore().setModel(oModel, "conceptDetailModel");
		   			console.log("Connection : getConceptDetailAjax()");
		   		},
		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
		   		error : function(error) {
		   			sap.m.MessageToast.show("Connexion error");
		   			console.log("Connexion error : getConceptDetailAjax()");
		   			console.log(error);
		   		}
		   	});
		   	

		   	}
	    	sap.ui.core.BusyIndicator.hide();

	   },
	   

	   getNewConceptMapperAjax : function (sVal) {
	   	
	    	sap.ui.core.BusyIndicator.show(0);

	    	var URL_concept = "http://localhost:5000/concept/";
	   	
	   	
		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		   	}
		   	
		   	
		   	if (sVal !== undefined) {
		   		return $.ajax({
		   		type:"GET",
		   		url: URL_concept + sVal,
		   		dataType:"json",
		   		async: false,
		   		header: {
		   			"Content-Type": "application/json",
		   		},
		   		success: function(data, response, xhr) {
		   			data.status = 'None';
		   			data.is_mapped = 'Not-Mapped';
		   			if (data.relations) {
		   				for (var i in data.relations) {
		   					if (data.relations[i].invalid_reason === "Valid") {
		   						data.status = 'Success';
		           				data.is_mapped = "Mapped";
		           				break;
		   					}
		   					
		   				}
		   			}
		   			console.log("Connection : getConceptDetailAjax()");
		   		},
		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
		   		error : function(error) {
		   			sap.m.MessageToast.show("Connexion error");
		   			console.log("Connexion error : getConceptDetailAjax()");
		   			console.log(error);
		   		}
		   		}).responseJSON
		   	}
	    	sap.ui.core.BusyIndicator.hide();

		   }



   };
});