sap.ui.define([], function() {
   "use strict";

   return {
	   


	   getConceptsAjax : function (oData) {
		   
		   debugger;
	    	sap.ui.core.BusyIndicator.show(0);

	    	var URL_concepts = "http://localhost:5000/concepts";

		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		   	}
		   	
		   	$.ajax({
		   		type:"GET",
		   		url:URL_concepts,
		   		dataType:"json",
		   		data: {"json" : JSON.stringify(oData)},
		   		async: false,
		   		header: {
		   			"Content-Type": "application/json",
		   		},
		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
		   		success: function(data) {
		   	        
		   			
		   			for (var i = 0; i < data.docs.length; i++) {
		   				var concept = data.docs[i];
		   				if (concept.concept_id >= 2000000000 && concept.is_mapped == 'S') {
		   					concept.status = 'Success';
		   				} else if (concept.concept_id >= 2000000000 && 
		   						(concept.is_mapped == 'NS' || concept.is_mapped == 'R')){
		   					concept.status = 'Warning';
		   				} else {
		   					concept.status = 'None';
		   				}
		   				
		   				if (!concept.frequency) {
		   					concept.frequency = sap.ui.getCore().getModel('i18n').getProperty('no_info');
		   				}
		   				
		   				if (concept.standard_concept === "S") {
		   					concept.standard_concept = "Standard";
		   				} else if (concept.standard_concept === "C") {
		   					concept.standard_concept = "Classification";
		   				} else {
		   					concept.standard_concept = "Non-Standard";
		   				}
		   				
		   				if (concept.invalid_reason === "U") {
		   					concept.invalid_reason = "Updated";
		   				} else if (concept.invalid_reason === "D") {
		   					concept.invalid_reason = "Deleted";
		   				} else {
		   					concept.invalid_reason = "Valid";
		   				}
		   				
		   			}
		   						
		   			data.search_string = oData.search_string;
	
		   			
		   			var oModel = new sap.ui.model.json.JSONModel(data);
		   			sap.ui.getCore().setModel(oModel, "conceptsModel");
		   			
		   			
		   			
		   			
		   			
		   			//reset checkbox to not selected
		   			var oTable_ref = sap.ui.getCore().byId('oTable');
		   			for (var i in oTable_ref.getAggregation('items')) {
		   				oTable_ref.getAggregation('items')[i].getAggregation('cells')[0].resetProperty('selected') 
		   			}
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   //// FACET
		   			
		   			var facets = ['domain_id', 'vocabulary_id', 'm_project_type_id', 'm_language_id', ];
		   			for (var i in facets) {
		   				var items = [];
		   				for (var idx in data.facet_fields[facets[i]]) {
		   					var item = String(Object.keys(data.facet_fields[facets[i]][idx]));
	
		   					var toShow = item;
		   					if (data.facet_fields[facets[i]][idx][item] !== 0) {
		   						toShow += " (" + String(data.facet_fields[facets[i]][idx][item]) + ")";
		   					}
		   						items.push({
		   							"key": item,
		   							"text": toShow ,
		   						});	
	//	   					}
	
		   				}
		   				var selected = '/' + facets[i];
		   				debugger;
		   				var oModel = new sap.ui.model.json.JSONModel();
		   				var mData = {					
		   					"selected" : sap.ui.getCore().getModel('selectedParams').getProperty(selected),
		   					"items" : items
		   				};
		   				
		   				oModel.setData(mData);
		   				sap.ui.getCore().setModel(oModel, facets[i]);
		   			}
		   			
		   			
		   			
		   			
		   			// standard_concept		
		   			var items = [];
		   			for (var idx in data.facet_fields.standard_concept) {
		   				var item = String(Object.keys(data.facet_fields.standard_concept[idx]));
		   				var toShowKey;
		   				if (item.toUpperCase() === "S") {
		   					toShowKey = "Standard";
		   				} else if (item.toUpperCase() === "C") {
		   					toShowKey = "Classification";
		   				} else {
		   					toShowKey = "Non-Standard";
		   				}
	
	//	   				to_show = to_show + " (" + String(data.facet_fields.standard_concept[idx][item]) + ")";
	//	   				if (data.facet_fields.standard_concept[idx][item] !== 0) {
		   				var toShowText = toShowKey;
		   				if (data.facet_fields.standard_concept[idx][item] !== 0) {
		   					toShowText += " (" + String(data.facet_fields.standard_concept[idx][item]) + ")";
		   				}
		   					items.push({
		   						"key": toShowKey,
		   						"text": toShowText,
		   					});	
	//	   				}
	
		   			}
		   			
		   			var oModel = new sap.ui.model.json.JSONModel();
		   			var mData = {
		   				"selected" : sap.ui.getCore().getModel('selectedParams').getProperty('/standard_concept'),
		   				"items" : items
		   			};
		   			oModel.setData(mData);
		   			sap.ui.getCore().setModel(oModel, 'standard_concept');
		   			
		   			
		   			// invalid_reason	
		   			var items = [];
		   			for (var idx in data.facet_fields.invalid_reason) {
		   				var item = String(Object.keys(data.facet_fields.invalid_reason[idx]));
		   				var toShowKey;
	
	
		   				if (item.toUpperCase() === "U") {
		   					toShowKey = "Updated";
		   				} else if (item.toUpperCase() === "D") {
		   					toShowKey = "Deleted";
		   				} else {
		   					toShowKey = "Valid";
		   				}
	
		   				var toShowText = toShowKey;
		   				if (data.facet_fields.invalid_reason[idx][item] !== 0) {
		   					toShowText += " (" + String(data.facet_fields.invalid_reason[idx][item]) + ")";
		   				}
	//	   				to_show = to_show + " (" + String(data.facet_fields.invalid_reason[idx][item]) + ")";
	//	   				if (data.facet_fields.invalid_reason[idx][item] !== 0) {
		   					items.push({
		   						"key": toShowKey,
		   						"text": toShowText,
		   					});	
	//	   				}
	
		   			}
		   			var oModel = new sap.ui.model.json.JSONModel();
		   			var mData = {
		   				"selected" : sap.ui.getCore().getModel('selectedParams').getProperty('/invalid_reason'),
		   				"items" : items
		   			};
		   			oModel.setData(mData);
		   			sap.ui.getCore().setModel(oModel, 'invalid_reason');
		   			
		   			
		   			
		   			
		   			
		   			var items = [];
		   			debugger;
		   			for (var idx in data.facet_fields.is_mapped) {
		   				var item = String(Object.keys(data.facet_fields.is_mapped[idx]));
		   				var toShowKey;
	
		   				if (item.toUpperCase() === "S") {
		   					toShowKey = "Mapped";
		   	
		   				} else {
		   					toShowKey = "Not-Mapped";
		   				}
		   				debugger;
		   				
		   				var toShowText = toShowKey;
		   				if (data.facet_fields.is_mapped[idx][item] !== 0) {
		   					toShowText += " (" + String(data.facet_fields.is_mapped[idx][item]) + ")";
		   				}
		   				
	//	   				to_show = to_show + " (" + String(data.facet_fields.invalid_reason[idx][item]) + ")";
	//	   				if (data.facet_fields.invalid_reason[idx][item] !== 0) {
		   					items.push({
		   						"key": toShowKey,
		   						"text": toShowText,
		   					});	
	//	   				}
	
		   			}
		   			var oModel = new sap.ui.model.json.JSONModel();
		   			var mData = {
		   				"selected" : sap.ui.getCore().getModel('selectedParams').getProperty('/is_mapped'),
		   				"items" : items
		   			};
		   			oModel.setData(mData);
		   			sap.ui.getCore().setModel(oModel, "is_mapped");
		   			
	
		   			
		   			
		   			
		   			// m_project_type_id for reviewer		
//		   			var items = [];
//		   			for (var idx in data.facet_fields.m_project_type_id) {
//		   				var item = String(Object.keys(data.facet_fields.m_project_type_id[idx]));
//		   			
//	
//		   				items.push({
//		   					"key": item,
//		   					"text": item,
//		   				});	
//	
//		   			} 
//		   			var oModel = new sap.ui.model.json.JSONModel();
//		   			var mData = {
//		   				"selected" : sap.ui.getCore().getModel('selectedParams').getProperty('/m_project_type_id_reviewer'),
//		   				"items" : items
//		   			};
//		   			oModel.setData(mData);
//		   			sap.ui.getCore().setModel(oModel, 'm_project_type_id_reviewer');
	
		   			
		   // FIN FACET
		   			
		   			
		   			
	
		   			console.log("Connection : getConceptsAjax()");
		   		},
		   		error : function(error) {		
		   			sap.m.MessageToast.show("Connexion error");
		   			console.log("Connexion error : getConceptsAjax()");
		   			console.log(error);
	
		   		}
		   	});
		   	
	    	sap.ui.core.BusyIndicator.hide();

	   },





	   getConceptsMapperAjax :function (oData) {
		   
	    	sap.ui.core.BusyIndicator.show(0);

	    	var URL_concepts = "http://localhost:5000/concepts";
	   	
		   	
		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		   	}
		   		
		   	$.ajax({
		   		type:"GET",
		   		url:URL_concepts,
		   		dataType:"json",
		   		data: {"json" : JSON.stringify(oData)},
		   		async: false,
		   		header: {
		   			"Content-Type": "application/json",
		   		},
		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
	
		   		success: function(data) {
		   			
		   			debugger;
		   			
		   			for (var i = 0; i < data.docs.length; i++) {
		   			
		   				var concept = data.docs[i];
		   				concept.concept_name = concept.concept_name.replace(/<strong>/g, '').replace(/<\/strong>/g, '');
		   				
		   				if (!concept.frequency) {
		   					concept.frequency=sap.ui.getCore().getModel('i18n').getProperty('no_info');
		   				}
		   				
		   				
		   				if (concept.standard_concept === "S") {
		   					concept.standard_concept = "Standard";
		   				} else if (concept.standard_concept === "C") {
		   					concept.standard_concept = "Classification";
		   				} else {
		   					concept.standard_concept = "Non-Standard";
		   				}
	
		   			}
		   			
		   			
		   			
		   			var facets = ['domain_id', 'vocabulary_id'];
		   			for (var i in facets) {
		   				var items = [];
		   				for (var idx in data.facet_fields[facets[i]]) {
		   					var item = String(Object.keys(data.facet_fields[facets[i]][idx]));
	
		   					var toShow = item;
		   					if (data.facet_fields[facets[i]][idx][item] !== 0) {
		   						toShow += " (" + String(data.facet_fields[facets[i]][idx][item]) + ")";
		   					}
		   						items.push({
		   							"key": item,
		   							"text": toShow ,
		   						});	
	//	   					}
	
		   				}
		   				var selected = '/' + facets[i];
		   				debugger;
		   				var oModel = new sap.ui.model.json.JSONModel();
		   				var mData = {					
		   					"selected" : sap.ui.getCore().getModel('searchFieldMapperModel').getProperty(selected),
		   					"items" : items
		   				};
		   				
		   				oModel.setData(mData);
		   				var modelName = facets[i] + '_mapper';
		   				sap.ui.getCore().setModel(oModel, modelName);
		   			}
		   			
		   			// standard_concept		
		   			var items = [];
		   			for (var idx in data.facet_fields.standard_concept) {
		   				var item = String(Object.keys(data.facet_fields.standard_concept[idx]));
		   				var toShowKey;
		   				if (item.toUpperCase() === "S") {
		   					toShowKey = "Standard";
		   				} else if (item.toUpperCase() === "C") {
		   					toShowKey = "Classification";
		   				} else {
		   					toShowKey = "Non-Standard";
		   				}
	
	//	   				to_show = to_show + " (" + String(data.facet_fields.standard_concept[idx][item]) + ")";
	//	   				if (data.facet_fields.standard_concept[idx][item] !== 0) {
		   				var toShowText = toShowKey;
		   				if (data.facet_fields.standard_concept[idx][item] !== 0) {
		   					toShowText += " (" + String(data.facet_fields.standard_concept[idx][item]) + ")";
		   				}
		   					items.push({
		   						"key": toShowKey,
		   						"text": toShowText,
		   					});	
	//	   				}
	
		   			}
		   			var oModel = new sap.ui.model.json.JSONModel();
		   			var mData = {
		   				"selected" : sap.ui.getCore().getModel('searchFieldMapperModel').getProperty('/standard_concept'),
		   				"items" : items
		   			};
		   			oModel.setData(mData);
		   			sap.ui.getCore().setModel(oModel, 'standard_concept_mapper');
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			
		   			debugger;
		   			
		   			
		   			
		   			
		   			var oModel = new sap.ui.model.json.JSONModel(data);
		   			sap.ui.getCore().setModel(oModel, "conceptsMapperModel");
	
		   			console.log("connection : getConceptsMapperAjax()");
		   		},
		   		error : function(error) {	
		   			sap.m.MessageToast.show("Connexion error");
		   			console.log("Connexion error : getConceptsMapperAjax()");
		   			console.log(error);
		   		}
		   	});
	    	sap.ui.core.BusyIndicator.hide();

	   }

	   
	   
   };
});