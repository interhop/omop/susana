sap.ui.define([	
	"../utils/keyboardEvents",
	
], function(keyboardEvents) {
   "use strict";

   return {
	   

	   
	   


	   postLoginAjax : function (oData) {
		
			var URL_login = "http://localhost:5000/login";

            $.ajax({
    			type:"POST",
    			url: URL_login,
    			dataType:"json",
    			data: oData,
    			async: false,
    			header: {
    				"Content-Type": "application/json",
    			},
    			success: function(data, response, xhr) {     
    				
    				
                	
        			var oModel = new sap.ui.model.json.JSONModel(data);
        			sap.ui.getCore().setModel(oModel, "loginModel");
        			
    				
    				
    				
//    				enable searchPage
    				if (data.m_is_admin === 'Admin') {
        				sap.ui.getCore().byId("usersParam").setVisible(true);
    				}
    				sap.ui.getCore().byId("oSearchFielGoToPage").setEnabled(true);
    				sap.ui.getCore().byId("oRowInPageInput").setEnabled(true);
    				sap.ui.getCore().byId("searchField").setEnabled(true);
    				sap.ui.getCore().byId("mapper").setEnabled(true);
    				sap.ui.getCore().byId("hideShowFilter").setEnabled(true);
    				sap.ui.getCore().byId("sortSearchPage").setEnabled(true);
    				sap.ui.getCore().byId("customizeSearchPage").setEnabled(true);
    				sap.ui.getCore().byId("registrationSearch").setVisible(false);
    				sap.ui.getCore().byId("loginSearch").setVisible(true);
    				sap.ui.getCore().byId("logoutSearch").setVisible(true);
    				sap.ui.getCore().byId("numFound").setVisible(true);
    				sap.ui.getCore().byId("switchToReviewer").setEnabled(true);
    				
    				
    				
    				
    				
    				
		   			// m_project_type_id for reviewer		
		   			var items = [];
		   			for (var idx in data.m_project_type_id) {
		   			
		   				if (data.m_project_type_id[idx] != 'OMOP') {
		   						items.push({
        		   					"key": data.m_project_type_id[idx],
        		   					"text": data.m_project_type_id[idx],
        		   				});
		   				}
		   					
	
		   			} 
		   			var oModel = new sap.ui.model.json.JSONModel();
		   			var mData = {
		   				"selected" : items, //sap.ui.getCore().getModel('selectedParams').getProperty('/m_project_type_id_reviewer'),
		   				"items" : items
		   			};
		   			oModel.setData(mData);
		   			sap.ui.getCore().setModel(oModel, 'm_project_type_id_reviewer');


    				
    				console.log("Connexion : login()");
    				
    				
                	sap.ui.controller("conceptmapper.searchPage").searcher();
               	
                	// active keyboard
                	keyboardEvents.searchPage();

    				
    			},
    			error : function(error) {
    				sap.m.MessageToast.show("Authentification error");
    				console.log("Connexion error : login()");
    				console.log(error);
    				
    				
    				keyboardEvents.initiationApp();
    				
    			}
    		});		  
            
	   },

	   
	   
	   
	   

   };
});