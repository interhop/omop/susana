sap.ui.define([], function() {
   "use strict";

   return {




	   voteAjax : function (sConceptRelationshipId, oData, sVerbAjax) {
		   
	    	sap.ui.core.BusyIndicator.show(0);

	    	var URL_vote = "http://localhost:5000/vote/";

		   	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
		   		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		   	}
		   	
		   	$.ajax({
		   		type: sVerbAjax,
		   		url: URL_vote + sConceptRelationshipId,
		   		dataType:"json",
		   		data: oData,
		   		async: false,
		   		header: {
		   			"Content-Type": "application/json",
		   		},
		   		beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
		   		success: function(data) {	
		   			console.log("Connection : voteAjax()");
		   		},
		   		error : function(error) {
		   			sap.m.MessageToast.show("Connexion error");
		   			console.log("Connexion error : voteAjax()");
		   			console.log(error);
		   		}
		   		
		   		
		   	});
		   	
	    	sap.ui.core.BusyIndicator.hide();

		 }

   
	   
	   
   };
});